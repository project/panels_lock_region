CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* Maintainers

INTRODUCTION
------------

Panels Lock Region is a Panels display renderer plugin which adds full region
lock functionality to Panels In-Place Editor. It allows users with certain
permission to lock/unlock entire regions on a Panel, so other users that can
use Panels IPE are able to edit panes arrangement, but only for those regions
that are not locked.

 * For a full description of the module, visit the project page:
   https://drupal.org/project/panels_lock_region

 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/panels_lock_region


REQUIREMENTS
------------

This module requires the following modules:
 * Panels (https://drupal.org/project/panels)
 * Panels In-Place Editor (distributed with panels)


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


CONFIGURATION
-------------

The module has no menu or modifiable settings. There is no configuration.
Just use it as a renderer plugin of Panels.


MAINTAINERS
-----------

Current maintainers:
 * Zequi Vázquez (rabbitlair) - https://www.drupal.org/u/rabbitlair
